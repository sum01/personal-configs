# 3.12 for cxx_std_20 3.20 for cxx_std_23 3.25 for cxx_std_26
cmake_minimum_required(VERSION 3.12.0 FATAL_ERROR)
project(
  REPLACETHIS
  VERSION 1.0.0
  LANGUAGES CXX
  HOMEPAGE_URL "https://your.project.homepage.here"
  DESCRIPTION "A default project template.")

# A Cmake-specific variable for enabling static or shared library building This
# value is used automatically when calling add_library()
option(BUILD_SHARED_LIBS "Build the library as shared (.so or .dll)" ON)

if(BUILD_SHARED_LIBS AND WIN32)
  # Necessary for Windows if building shared libs Avoids needing to add
  # __declspec(dllexport) everywhere See https://stackoverflow.com/a/40743080
  set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS ON)
endif()

include(GNUInstallDirs)
# This is a simple way to change the includes dir for your installed project So
# you could do /usr/include/projectname/subproject/whatever.hpp
set(_project_public_include_dir "${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}")

# Creates our empty library target Not adding files lets us use target_sources()
# to control file scope
add_library(${PROJECT_NAME} src/main.cpp)
# Used for people building in-tree so they can link against libep::ep
add_library(${PROJECT_NAME}::${PROJECT_NAME} ALIAS ${PROJECT_NAME})
# target_sources is more proper for a library because you can control
# PUBLIC/PRIVATE/INTERFACE This scope control lets us propogate file headers to
# anything using this lib

include(CheckIPOSupported)
# Checks if their compiler has LTO
check_ipo_supported(
  RESULT _has_ipo
  OUTPUT _ipo_error
  LANGUAGES CXX)
# Enable LTO if you have it.
if(_has_ipo)
  # Enables LTO/IPO support on release-builds only
  set_target_properties(
    ${PROJECT_NAME} PROPERTIES $<$<CONFIG:Release>:INTERPROCEDURAL_OPTIMIZATION
                               TRUE>)
endif()

# Works for MSVC and (most) non-MSVC compilers
target_compile_options(
  ${PROJECT_NAME}
  PRIVATE # These flags enable all warnings
          $<IF:$<CXX_COMPILER_ID:MSVC>,/W4 /WX,-Wall -Wextra -pedantic -Werror>)

target_include_directories(
  ${PROJECT_NAME} PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src>
                         $<INSTALL_INTERFACE:${_project_public_include_dir}>)

# Target the build for C++20
target_compile_features(${PROJECT_NAME} PRIVATE cxx_std_20)

# Creates an install target for easy install
install(
  TARGETS ${PROJECT_NAME}
  EXPORT ${PROJECT_NAME}Targets
  # Both of these calls are needed since a user can build as static OR shared
  # libraries
  ARCHIVE DESTINATION "${CMAKE_INSTALL_LIBDIR}"
  LIBRARY DESTINATION "${CMAKE_INSTALL_LIBDIR}"
          # This tells cmake where to install the public headers we attached to
          # the example_class
  PUBLIC_HEADER DESTINATION "${_project_public_include_dir}")

# Cmake's find_package search path is different based on the system. See
# https://cmake.org/cmake/help/latest/command/find_package.html for the list.
# See https://gitlab.kitware.com/cmake/cmake/-/issues/18453 for discussion on a
# future replacement of this.
if(CMAKE_SYSTEM_NAME STREQUAL "Windows")
  set(_TARGET_INSTALL_CMAKEDIR "${CMAKE_INSTALL_PREFIX}/cmake/${PROJECT_NAME}")
else()
  # On Non-Windows, it should be /usr/lib/cmake/<name>/<name>Config.cmake NOTE:
  # This may or may not work for macOS...
  set(_TARGET_INSTALL_CMAKEDIR "${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}")
endif()

# This actually installs the export we created Note that it should be the name
# of the EXPORT in the previous install(TARGETS) call
install(
  EXPORT ${PROJECT_NAME}Targets
  # Puts the library into a namespace spn (some_project_name)
  NAMESPACE spn
  # You could realistically install it anywhere, but the libdir is good for
  # find_package
  DESTINATION "${_TARGET_INSTALL_CMAKEDIR}/${PROJECT_NAME}Targets")

include(CMakePackageConfigHelpers)

# Configures the meta-file httplibConfig.cmake.in to replace variables with
# paths/values/etc.
configure_package_config_file(
  "${PROJECT_NAME}Config.cmake.in"
  "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Config.cmake"
  INSTALL_DESTINATION "${_TARGET_INSTALL_CMAKEDIR}"
  PATH_VARS CMAKE_INSTALL_FULL_INCLUDEDIR
  # There aren't any components, so don't use the macro
  NO_CHECK_REQUIRED_COMPONENTS_MACRO)

write_basic_package_version_file(
  ${PROJECT_NAME}ConfigVersion.cmake
  VERSION ${CMAKE_PROJECT_VERSION}
  # SameMajorVersion is essentially SemVer
  COMPATIBILITY SameMajorVersion)

install(FILES "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Config.cmake"
              "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}ConfigVersion.cmake"
        DESTINATION ${_TARGET_INSTALL_CMAKEDIR})

# Does all the Cpack setup for us
include(cmake/setup_cpack.cmake)

include(CTest)
# BUILD_TESTING is default ON and declared inside of CTest
if(BUILD_TESTING)
  add_subdirectory(test)
endif()
