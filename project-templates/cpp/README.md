# Placeholder

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin non sagittis lorem. Morbi nec lorem magna. Maecenas tristique, orci nec sollicitudin consectetur, enim nulla auctor diam, bibendum varius metus velit sed risus. Vivamus hendrerit erat augue, ac interdum eros luctus sed. Aenean ac aliquet ante. Aliquam quis finibus magna. Duis eget justo vel diam ullamcorper dapibus in sit amet quam. Suspendisse vehicula cursus risus, eget consequat dui suscipit vel.

## Dependencies

[Cmake](https://cmake.org/) v3.8 (or newer) is needed for building.

A C++11 compatible compiler is also needed.

## Build & Install

Note that if you don't want to install, remove `--target install` from the last command.  
If you do that, you can also run as non-privledged (i.e. no `sudo` or `runas`)

Linux/macOS:

```
mkdir -p build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTING=OFF ..
sudo cmake --build . --target install
```

Windows:

```
mkdir build
cd build
cmake -DBUILD_TESTING=OFF ..
runas /user:Administrator "cmake --build . --config Release --target install"
```
