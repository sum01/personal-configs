# To prevent Alacritty from being the wrong size on different DPI monitors
WINIT_X11_SCALE_FACTOR=1
# Disables telemetry on dotnet
DOTNET_CLI_TELEMETRY_OPTOUT=1
# Personal preferences...
BROWSER=firefox
EDITOR=nvim
VISUAL=$EDITOR
