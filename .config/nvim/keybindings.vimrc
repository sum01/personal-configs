" Remap split movement to a single key instead of having to use it with CTRL-W
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
" Remap's the 'move block/line' command to work with keybindings
nnoremap <A-j> :m .+1<CR>==
nnoremap <A-k> :m .-2<CR>==
inoremap <A-j> <Esc>:m .+1<CR>==gi
inoremap <A-k> <Esc>:m .-2<CR>==gi
vnoremap <A-j> :m '>+1<CR>gv=gv
vnoremap <A-k> :m '<-2<CR>gv=gv
" Keybind 'copy selection' to system clipboard
vmap <C-c> "+y
" Keybind the terminal-mode exit keys to Escape
tnoremap <Esc> <C-\><C-n>

" Quick keybind to hide current highlighting
nnoremap <silent> <Space> :noh<CR>
" Quick keybind to toggle highlighting from a search on/off
"nnoremap <silent> <Space> :set hlsearch! hlsearch?<CR>

" A one-liner to build & test any Cmake project
"command! CmakeBuild :!bash -c 'mkdir -p build && cd build && cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=ON .. && cp -f compile_commands.json ../ && cmake --build . -- -j$(nproc) && if [[ -d "$(pwd)/Testing" ]]; then ctest -C Debug; fi; cd ..'
