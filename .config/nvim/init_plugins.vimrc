" Auto-installs vim-plug if not installed
if !has("win32")
	if empty(glob('~/.config/nvim/autoload/plug.vim'))
		silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
		autocmd VimEnter * PlugInstall
	endif
endif

" All the plugins that are managed by Plug
" Things without the tag setting either don't have tags, or their tags are very out of date
call plug#begin()
	" A decent monokai colortheme
	Plug 'crusoexia/vim-monokai'
	" Editorconfig support
	Plug 'editorconfig/editorconfig-vim', {
			\ 'tag': '*',
			\ }
	" A code auto-formatter to use things like Prettier or Uncrustify
	Plug 'Chiel92/vim-autoformat'
	" A really nice statusline replacement
	Plug 'vim-airline/vim-airline', {
			\ 'tag': '*',
			\ }
	" Adds a bunch of themes to the vim-airline plugin
	" Has the 'minimalist' theme I like
	Plug 'vim-airline/vim-airline-themes'
	" Adds a nice file-tree viewer
	Plug 'preservim/nerdtree', {
			\ 'tag': '*',
			\ }
	" Provides an i3-esque resize/move mode for vimsplits
	Plug 'simeji/winresizer'
	" Gives us a live LaTeX preview
	" Only bother loading when working on TeX files
	Plug 'xuhdev/vim-latex-live-preview', {
				\ 'for': 'tex',
				\ }
	" Provides things like autocompletion and language server integration
	" \ 'tag': '*',
	Plug 'neoclide/coc.nvim', {
				\ 'branch': 'release',
				\ }
	" For coc.nvim & Python-LSP
	" see https://github.com/yaegassy/coc-pylsp
	Plug 'yaegassy/coc-pylsp', {'do': 'yarn install --frozen-lockfile'}
	" Adds better syntax highlighting to i3 config
	Plug 'mboughaba/i3config.vim', {
				\ 'for': 'i3config',
				\ }
	" Lets you change whatever brackets/braces/quotes are surrounding some text
	" uses commands like cs'" to change ' to a "
	Plug 'tpope/vim-surround'
	" Lets you easily comment lines out with commands like gc
	Plug 'tpope/vim-commentary'
	" Lets you use . to repeat plugin commands (like from vim-surround)
	Plug 'tpope/vim-repeat'
call plug#end()
