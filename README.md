A repo to backup my settings and config files for various text-editor's, code formatters, etc.

Below is just some notes on new install configuration and whatnot.

# New Arch install

Get system-specific stuff

1. Get GPU drivers
   - AMD: `mesa libva-mesa-driver mesa-vdpau vulkan-radeon`
   - Nvidia: Specific to the card, look it up.
2. `intel-ucode` or `amd-ucode`

Then run the [setup_new_arch](./scripts/setup_new_arch) script to download common programs.  
After that, symlink configs and other files with the [symlink_confs](./scripts/symlink_confs) script.

## Display

Use arandr to setup monitor(s).  
Save the config in `/etc/sddm.conf.d/setup_screens` and edit in refresh rates using `-r 60` (or whatever number).

## Sound

Use pavucontrol (pulse audio control) or alsamixer to setup audio, if needed.  
Use `alsamixer` to pick the correct soundcard settings, then `alsactl store` to save it.

## Other

Enable (uncomment) the `[multilib]` repo (if not already enabled) in `/etc/pacman.conf`

Put `~/.ssh/config` and `~/.ssh/known_hosts` in place, then fix SSH permissions (if needed):

```bash
chmod 700 ~/.ssh
chmod 600 ~/.ssh/*
chmod 644 ~/.ssh/*.pub
chmod 644 ~/.ssh/authorized_keys ~/.ssh/config ~/.ssh/known_hosts
```

Don't forget to `sudo pacman -Fy` & import gpg backup.

Get [texmaths](https://extensions.libreoffice.org/extensions/texmaths-1) and [code highlighter plugin](https://extensions.libreoffice.org/en/extensions/show/coooder) for LibreOffice.  
`yay -S libreoffice-extension-texmaths libreoffice-extension-code-highlighter`
